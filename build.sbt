// *****************************************************************************
// Projects
// *****************************************************************************

lazy val databaseSchemaExtractor =
  project
    .in(file("."))
    .enablePlugins(AutomateHeaderPlugin, JavaAppPackaging)
    .configs(IntegrationTest)
    .settings(settings)
    .settings(
      Defaults.itSettings,
      headerSettings(IntegrationTest),
      inConfig(IntegrationTest)(org.scalafmt.sbt.ScalafmtPlugin.scalafmtConfigSettings),
      inConfig(IntegrationTest)(scalafmtSettings),
      IntegrationTest / console / scalacOptions --= Seq(
        "-Xfatal-warnings",
        "-Ywarn-unused-import",
        "-Ywarn-unused:implicits",
        "-Ywarn-unused:imports",
        "-Ywarn-unused:locals",
        "-Ywarn-unused:params",
        "-Ywarn-unused:patvars",
        "-Ywarn-unused:privates"
      ),
      IntegrationTest / parallelExecution := false,
    )
    .settings(
      libraryDependencies ++= Seq(
        library.catsCore,
        library.catsEffect,
	library.decline,
	library.declineEffect,
	library.declineRefined,
        library.derby,
        library.derbyShared,
        library.dfasdlCore,
        library.enumeratumCore,
        library.firebird,
        library.h2,
        library.hsqldb,
        library.mariadb,
        library.mysql,
        library.oracle,
        library.oracleI18N,
        library.postgresql,
        library.refinedCats,
        library.refinedCore,
        library.simulacrum,
        library.sqlite,
        library.sqlserver,
        library.refinedScalaCheck % IntegrationTest,
        library.scalaCheck        % IntegrationTest,
        library.scalaTest         % IntegrationTest,
        library.scalaTestPlus     % IntegrationTest,
	library.xmlunit           % IntegrationTest,
        library.refinedScalaCheck % Test,
        library.scalaCheck        % Test,
        library.scalaTest         % Test,
        library.scalaTestPlus     % Test,
	library.xmlunit           % Test
      )
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      val cats          = "2.7.0"
      val catsEffect    = "3.2.9"
      val derby         = "10.15.2.0"
      val decline       = "2.2.0"
      val dfasdlCore    = "2.0.2"
      val enumeratum    = "1.7.0"
      val firebird      = "3.0.11"
      val h2            = "1.4.200"
      val hsqldb        = "2.6.1"
      val mariadb       = "2.7.4"
      val mysql         = "8.0.27"
      val oracle        = "19.3.0.0"
      val postgresql    = "42.3.1"
      val refined       = "0.9.28"
      val scalaCheck    = "1.15.4"
      val scalaTest     = "3.2.10"
      val scalaTestPlus = "3.2.10.0"
      val simulacrum    = "1.0.1"
      val sqlite        = "3.32.3.3" // Test before upgrade for `java.lang.UnsatisfiedLinkError: 'int org.sqlite.core.NativeDB.limit(int, int)'` error!!!
      val sqlserver     = "8.4.1.jre11"
      val xmlunit       = "2.8.3"
    }
    val catsCore          = "org.typelevel"           %% "cats-core"            % Version.cats
    val catsEffect        = "org.typelevel"           %% "cats-effect"          % Version.catsEffect
    val decline           = "com.monovore"            %% "decline"              % Version.decline
    val declineEffect     = "com.monovore"            %% "decline-effect"       % Version.decline
    val declineRefined    = "com.monovore"            %% "decline-refined"      % Version.decline
    val derby             = "org.apache.derby"        %  "derby"                % Version.derby
    val derbyShared       = "org.apache.derby"        %  "derbyshared"          % Version.derby
    val dfasdlCore        = "org.dfasdl"              %% "dfasdl-core"          % Version.dfasdlCore
    val enumeratumCore    = "com.beachape"            %% "enumeratum"           % Version.enumeratum
    val firebird          = "org.firebirdsql.jdbc"    %  "jaybird-jdk18"        % Version.firebird
    val h2                = "com.h2database"          %  "h2"                   % Version.h2
    val hsqldb            = "org.hsqldb"              %  "hsqldb"               % Version.hsqldb
    val mariadb           = "org.mariadb.jdbc"        %  "mariadb-java-client"  % Version.mariadb
    val mysql             = "mysql"                   %  "mysql-connector-java" % Version.mysql
    val oracle            = "com.oracle.ojdbc"        %  "ojdbc8"               % Version.oracle
    val oracleI18N        = "com.oracle.ojdbc"        %  "orai18n"              % Version.oracle
    val postgresql        = "org.postgresql"          %  "postgresql"           % Version.postgresql
    val refinedCore       = "eu.timepit"              %% "refined"              % Version.refined
    val refinedCats       = "eu.timepit"              %% "refined-cats"         % Version.refined
    val refinedScalaCheck = "eu.timepit"              %% "refined-scalacheck"   % Version.refined
    val scalaCheck        = "org.scalacheck"          %% "scalacheck"           % Version.scalaCheck
    val scalaTest         = "org.scalatest"           %% "scalatest"            % Version.scalaTest
    val scalaTestPlus     = "org.scalatestplus"       %% "scalacheck-1-15"      % Version.scalaTestPlus
    val simulacrum        = "org.typelevel"           %% "simulacrum"           % Version.simulacrum
    val sqlite            = "org.xerial"              %  "sqlite-jdbc"          % Version.sqlite
    val sqlserver         = "com.microsoft.sqlserver" %  "mssql-jdbc"           % Version.sqlserver
    val xmlunit           = "org.xmlunit"             %  "xmlunit-core"         % Version.xmlunit
  }

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val settings =
  commonSettings ++
  scalafmtSettings

def compilerSettings(sv: String) =
  CrossVersion.partialVersion(sv) match {
    case _ =>
      Seq(
        "-deprecation",
        "-explaintypes",
        "-feature",
        "-language:_",
        "-unchecked",
        "-Xcheckinit",
        //"-Xfatal-warnings",
        "-Xlint:adapted-args",
        "-Xlint:constant",
        "-Xlint:delayedinit-select",
        "-Xlint:doc-detached",
        "-Xlint:inaccessible",
        "-Xlint:infer-any",
        "-Xlint:missing-interpolator",
        "-Xlint:nullary-unit",
        "-Xlint:option-implicit",
        "-Xlint:package-object-classes",
        "-Xlint:poly-implicit-overload",
        "-Xlint:private-shadow",
        "-Xlint:stars-align",
        "-Xlint:type-parameter-shadow",
        "-Ywarn-dead-code",
        "-Ywarn-extra-implicit",
        "-Ywarn-numeric-widen",
        "-Ywarn-unused:implicits",
        "-Ywarn-unused:imports",
        "-Ywarn-unused:locals",
        "-Ywarn-unused:params",
        "-Ywarn-unused:patvars",
        "-Ywarn-unused:privates",
        "-Ywarn-value-discard",
        "-Ycache-plugin-class-loader:last-modified",
        "-Ycache-macro-class-loader:last-modified",
        "-Ymacro-annotations" // Needed for Simulacrum
      )
  }

lazy val commonSettings =
  Seq(
    scalaVersion := "2.13.7",
    crossScalaVersions := Seq(scalaVersion.value),
    organization := "com.wegtam",
    organizationName := "Contributors as noted in the AUTHORS.md file",
    startYear := Some(2020),
    licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
    scalacOptions ++= compilerSettings(scalaVersion.value),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.2" cross CrossVersion.full),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    ),
    Compile / unmanagedSourceDirectories := Seq((Compile / scalaSource).value),
    Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any),
    Test / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    ),
    Test / unmanagedSourceDirectories := Seq((Test / scalaSource).value),
)

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true,
  )
