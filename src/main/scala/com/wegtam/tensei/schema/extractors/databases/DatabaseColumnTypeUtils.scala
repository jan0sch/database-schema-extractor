/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import simulacrum._

/** A typeclass for helpful operations regarding database column types.
  */
@typeclass trait DatabaseColumnTypeUtils[A] {

  /** Create a database column type from the given input.
    *
    * @param a An arbitrary type.
    * @return An option to the corresponding database column type.
    */
  def toType(a: A): Option[DatabaseColumnType]

}
