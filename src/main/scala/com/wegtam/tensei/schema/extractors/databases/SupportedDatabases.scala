/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import java.net.URI

import cats.implicits._
import enumeratum._
import scala.util.Try

/** A sealed trait for our supported databases.
  */
sealed trait SupportedDatabases extends EnumEntry with Product with Serializable

object SupportedDatabases extends Enum[SupportedDatabases] {
  val values = findValues

  case object Derby      extends SupportedDatabases
  case object Firebird   extends SupportedDatabases
  case object H2         extends SupportedDatabases
  case object HyperSQL   extends SupportedDatabases
  case object MariaDB    extends SupportedDatabases
  case object MySQL      extends SupportedDatabases
  case object Oracle     extends SupportedDatabases
  case object PostgreSQL extends SupportedDatabases
  case object SQLite     extends SupportedDatabases
  case object SQLServer  extends SupportedDatabases

  /** Return the supported database from the given URI which must contain
    * a valid JDBC conection string.
    *
    * @param u A URI containing a valid JDBC connection string.
    * @return An option to the supported database for the connection.
    */
  def fromURI(u: URI): Option[SupportedDatabases] =
    u.getSchemeSpecificPart.trim match {
      case "" => None
      case sp =>
        Try(new URI(sp)).toOption match {
          case None     => None
          case Some(su) =>
            // `URI.getScheme` might return `null` therefore we use `Option`
            // to avoid a possible `NullPointerException`.
            for {
              st <- Option(su.getScheme)
                .map(_.toLowerCase(java.util.Locale.ROOT).takeWhile(_ =!= ':'))
              db <- st match {
                case "derby"       => Derby.some
                case "firebirdsql" => Firebird.some
                case "h2"          => H2.some
                case "hsqldb"      => HyperSQL.some
                case "mariadb"     => MariaDB.some
                case "mysql"       => MySQL.some
                case "oracle"      => Oracle.some
                case "postgresql"  => PostgreSQL.some
                case "sqlite"      => SQLite.some
                case "sqlserver"   => SQLServer.some
                case _             => None
              }
            } yield db
        }
    }

}
