/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.h2

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {
  // TODO Map `UUID` and `JSON` to something useable.

  implicit val h2DatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BINARY" | "VARBINARY" | "BINARY VARYING" | "LONGVARBINARY" | "RAW" | "BYTEA" | "BLOB" |
              "BINARY LARGE OBJECT" | "TINYBLOB" | "MEDIUMBLOB" | "LONGBLOB" | "IMAGE" | "OID" =>
            Binary.some
          case "DATE" =>
            Date.some
          case "DATETIME" | "SMALLDATETIME" | "TIMESTAMP" | "TIMESTAMP WITHOUT TIME ZONE" =>
            DateTime.some
          case "DEC" | "DECIMAL" | "DOUBLE" | "DOUBLE PRECISION" | "FLOAT" | "FLOAT4" | "FLOAT8" | "NUMBER" |
              "NUMERIC" | "REAL" =>
            FormatNum.some
          case "BIGINT" | "BIT" | "BOOLEAN" | "BOOL" | "INT" | "INT2" | "INT4" | "INT8" | "INTEGER" | "MEDIUMINT" |
              "SIGNED" | "SMALLINT" | "TINYINT" | "YEAR" =>
            Num.some
          case "CHAR" | "CHARACTER" | "CHARACTER LARGE OBJECT" | "CHAR VARYING" | "CHARACTER VARYING" | "CLOB" |
              "LONG VARCHAR" | "LONGTEXT" | "MEDIUMTEXT" | "NCHAR" | "NCLOB" | "NTEXT" | "NVARCHAR" | "NVARCHAR2" |
              "TEXT" | "TINYTEXT" | "VARCHAR" | "VARCHAR2" | "VARCHAR_CASESENSITIVE" | "VARCHAR_IGNORECASE" =>
            Str.some
          case "TIME" | "TIME WITH TIME ZONE" | "TIME WITHOUT TIME ZONE" =>
            Time.some
          case _ => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
