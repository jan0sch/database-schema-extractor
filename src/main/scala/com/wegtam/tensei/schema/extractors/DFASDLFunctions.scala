/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import javax.xml.XMLConstants
import javax.xml.parsers.{ DocumentBuilder, DocumentBuilderFactory }
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import cats.implicits._
import com.wegtam.tensei.schema.extractors.databases._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.NonNegLong
import eu.timepit.refined.types.string.NonEmptyString
import org.dfasdl._

import scala.util.Try

object DFASDLFunctions {
  final val ID_ATTR: NonEmptyString                   = "id"
  final val DEFAULT_NUMERIC_SEPARATOR: NonEmptyString = "."
  final val GENERAL_FORMATNUM_REGEX: NonEmptyString   = "(-?[\\d\\.,⎖]+)"

  def createDocumentBuilder(): DocumentBuilder = {
    val xsd     = getClass().getResourceAsStream("/org/dfasdl/dfasdl.xsd")
    val factory = DocumentBuilderFactory.newInstance()
    factory.setValidating(false)
    factory.setNamespaceAware(true)
    val schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
    factory.setSchema(schemaFactory.newSchema(new StreamSource(xsd)))
    val builder = factory.newDocumentBuilder()
    builder
  }

  /** Create the regular expression for a `FORMATTED_NUMBER` element in
    * the DFASDL.
    *
    * @param separator The decimal separator character.
    * @param scale     An optional scale of the column (e.g. 2,34 means scale 2). It will be 0 in most cases.
    * @param precision An optional precision aka "length" of the column.
    * @return A regular expression for extracting formatted numbers.
    */
  def createFormatnumRegex(
      separator: NonEmptyString
  )(scale: Option[NonNegLong])(precision: Option[NonNegLong]): Option[NonEmptyString] = {
    val separatorSign: NonEmptyString =
      if (separator === ".")
        "\\."
      else
        ","
    (scale, precision) match {
      case (Some(scale), Some(precision)) =>
        NonEmptyString
          .from(
            s"(-?\\d{0,${(scale - precision).show}}${separatorSign.show}\\d{0,${precision.show}})"
          )
          .toOption
      case (Some(scale), None) => NonEmptyString.from(s"(-?\\d{1,${scale.show}})").toOption
      case (None, Some(precision)) =>
        NonEmptyString.from(s"(-?\\d*?${separatorSign.show}\\d{0,${precision.show}})").toOption
      case _ => GENERAL_FORMATNUM_REGEX.some
    }
  }

  /** Create the DFASDL element skeleton of a database table description.
    *
    * It will consist of a sequence element and a child element which
    * represents a row (i.e. the column descriptions).
    *
    * @param d A DFASDL document.
    * @param t A database table description model.
    * @return An option to an XML element that represents the outer skeleton of a DFASDL table.
    */
  def createTableSkeletonElement(
      d: org.w3c.dom.Document
  )(t: DatabaseTable): Option[org.w3c.dom.Element] =
    Try {
      val table = d.createElement(Element.SEQUENCE.tagName)
      table.setAttribute(ID_ATTR, t.name)
      if (t.primaryKey.nonEmpty) {
        table.setAttribute(Attribute.DB_PRIMARY_KEY.name, t.primaryKey.mkString(","))
      }
      val row           = d.createElement(Element.ELEMENT.tagName)
      val tname: String = t.name
      row.setAttribute(ID_ATTR, s"${tname}_row")
      val _ = table.appendChild(row)
      table
    }.toOption

  /** Return the first node from the given XML NodeList if it
    * exists.
    *
    * @param ns A list of nodes which might be empty.
    * @return An option to the first node.
    */
  def getFirstNode(ns: org.w3c.dom.NodeList): Option[org.w3c.dom.Node] = {
    val n = ns.item(0)
    if (n eq null)
      None
    else
      n.some
  }

  /** Initialise a DFASDL document and return it.
    *
    * @return A properly initialised DFASDL document.
    */
  def initDocument: org.w3c.dom.Document = {
    val b      = createDocumentBuilder()
    val d      = b.newDocument()
    val dfasdl = d.createElement(Element.ROOT.tagName)
    dfasdl.setAttribute("xmlns", "http://www.dfasdl.org/DFASDL")
    dfasdl.setAttribute(Attribute.SEMANTIC_SCHEMA.name, "custom")
    val _ = d.appendChild(dfasdl)
    d
  }

  /** Sanitize a given name (usually generated automatically) for usage
    * within the DFASDL. This process means the removal of all non word
    * characters, underscores and hyphens from the input.
    *
    * @param name The name to be sanitized.
    * @return An option to the sanitized name.
    */
  def sanitizeName(name: NonEmptyString): Option[NonEmptyString] =
    NonEmptyString.from(name.replaceAll("[^\\w_-]", "")).toOption

}
