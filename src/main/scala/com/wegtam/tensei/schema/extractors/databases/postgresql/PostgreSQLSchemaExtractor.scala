/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.postgresql

import java.sql.{ Connection, PreparedStatement }
import java.util.Locale

import cats.data.NonEmptyList
import cats.effect._
import cats.implicits._
import com.wegtam.tensei.schema.extractors._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnFlags._
import com.wegtam.tensei.schema.extractors.databases.postgresql.DatabaseColumnTypeUtilsInstances._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnTypeUtils.ops._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.NonNegLong
import org.dfasdl._

import scala.collection.mutable
import scala.util.Try

/** A buffer for the raw metadata of a table column extracted from the
  * query.
  *
  * @param columnName    The name of the column.
  * @param columnType    The type description of the column.
  * @param nullable      A flag indicating if the column is allowed to be null.
  * @param precision     The precision (length / size) of the column.
  * @param scale         The scale of the column (will mostly be zero).
  * @param autoIncrement The "auto increment" description of the column.
  * @param foreignKey    The foreign key definition of the column.
  * @param defaultValue  The default value of the column.
  */
final case class RawColumnMetadata(
    columnName: String,
    columnType: String,
    nullable: Boolean,
    precision: Option[Long],
    scale: Option[Long],
    autoIncrement: Option[String],
    foreignKey: Option[String],
    default: Option[String]
)

object PostgreSQLSchemaExtractor {

  /** Extract a schema description from a given database connection and
    * return it in the format of the Data Format and Semantics Description
    * Language (DFASDL).
    *
    * @param c      A database connection.
    * @param dbName The name of the database.
    * @return Either a DFASDL XML document or an error message.
    */
  def extractSchema(
      c: IO[Connection]
  )(dbName: DatabaseName): IO[Either[ErrorMessage, org.w3c.dom.Document]] = {
    val listTables   = prepareListTablesStatement(c)
    val tableDetails = prepareDetailStatements(c)
    val document = for {
      tables <- getTables(listTables)(dbName)
      dfasdl = DFASDLFunctions.initDocument
      tablesData <- tables.traverse(t => describeTable(dfasdl)(tableDetails)(t))
      _ = tablesData.foreach(_.foreach(t => dfasdl.getDocumentElement().appendChild(t)))
    } yield dfasdl.asRight[ErrorMessage]
    document
  }

  /** Create a specific DFASDL Data-Element that represents a column of an actual table. Depending
    * on the type of the column, the Data-Element is created with the appropriate tag name and
    * additional attributes.
    *
    * @param d          The XML document.
    * @param tableName  The name of the table.
    * @param flags      A list of flags for the column (auto increment, nullable) which might be empty.
    * @param fk         The foreign key definition for the column.
    * @param default    An optional string containing the default value for the column.
    * @param scale      An optional scale of the column (e.g. 2,34 means scale 2). It will be 0 in most cases.
    * @param precision  An optional precision aka "length" of the column.
    * @param columnType The type of the column in the database table.
    * @param columnName The name of the column in the database table.
    * @return An element that represents the column.
    */
  protected def createColumnElement(d: org.w3c.dom.Document)(tableName: DatabaseTableName)(
      flags: List[DatabaseColumnFlags]
  )(fk: Option[ForeignKey])(default: Option[String])(scale: Option[NonNegLong])(
      precision: Option[NonNegLong]
  )(
      columnType: DatabaseColumnTypeName
  )(columnName: DatabaseColumnName): Option[org.w3c.dom.Element] = {
    val element = columnType.toType.map { t =>
      t match {
        case DatabaseColumnType.Binary =>
          // FIXME Actually use the binary element here!
          val e = d.createElement(Element.STRING.tagName)
          precision.foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d.show))
          e
        case DatabaseColumnType.Date =>
          val e = d.createElement(Element.DATE.tagName)
          e
        case DatabaseColumnType.DateTime =>
          val e = d.createElement(Element.DATETIME.tagName)
          e
        case DatabaseColumnType.FormatNum =>
          val e = d.createElement(Element.FORMATTED_NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach { s =>
            e.setAttribute(Attribute.MAX_PRECISION.name, s.show)
            e.setAttribute(Attribute.DECIMAL_SEPARATOR.name, ".")
          }
          precision.foreach { p =>
            e.setAttribute(Attribute.MAX_DIGITS.name, p.show)
          }
          DFASDLFunctions
            .createFormatnumRegex(DFASDLFunctions.DEFAULT_NUMERIC_SEPARATOR)(precision)(scale)
            .foreach(r => e.setAttribute(Attribute.FORMAT.name, r.show))
          e
        case DatabaseColumnType.Num =>
          val e = d.createElement(Element.NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach(s => e.setAttribute(Attribute.PRECISION.name, s.show))
          precision.foreach(p => e.setAttribute(Attribute.MAX_DIGITS.name, p.show))
          e
        case DatabaseColumnType.Str =>
          val e = d.createElement(Element.STRING.tagName)
          precision
            .filterNot(_ === 0L)
            .foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d))
          e
        case DatabaseColumnType.Time =>
          val e = d.createElement(Element.TIME.tagName)
          e
      }
    }
    element.map { e =>
      e.setAttribute(DFASDLFunctions.ID_ATTR, s"${tableName.show}_row_${columnName.show}")
      e.setAttribute(Attribute.DB_COLUMN_NAME.name, columnName.show)
      flags
        .find(_ === DatabaseColumnFlags.AutoIncrement)
        .foreach(_ => e.setAttribute(Attribute.DB_AUTO_INCREMENT.name, "true"))
      fk.foreach(cs => e.setAttribute(Attribute.DB_FOREIGN_KEY.name, cs.show.toLowerCase(Locale.ROOT)))
      e
    }
  }

  /** Return a list of tables using the given prepared statement and
    * database name.
    *
    * @param s A prepared statement to list all tables.
    * @param n The name of the database.
    * @return A list of database table description which can be used to extract more details from the schema.
    */
  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  protected def getTables(s: IO[PreparedStatement])(n: DatabaseName): IO[List[DatabaseTable]] =
    s.map { stmt =>
      val ts = mutable.ListBuffer.empty[DatabaseTable]
      if (stmt.getParameterMetaData().getParameterCount() > 0) {
        stmt.setString(1, n)
      }
      val rs   = stmt.executeQuery()
      val meta = rs.getMetaData()
      while (rs.next()) {
        val tn = rs.getString("TABLE_NAME").toLowerCase(Locale.ROOT)
        val pk =
          if (meta.getColumnCount() > 1 && rs.getString("PK_COLUMNS") =!= null)
            rs.getString("PK_COLUMNS")
              .toLowerCase(Locale.ROOT)
              .split(",")
              .toList
              .sorted // We sort here because PostgreSQL returns them in arbitrary ordering.
          else
            List.empty
        DatabaseTable.from(tn, pk).foreach(t => ts += t)
      }
      ts.toList
    }

  /** Create a description of a database table within a PostgreSQL database.
    *
    * @param d          A DFASDL document that will be used for the table description.
    * @param statements A list prepared SQL statements that are used to extract necessary information from the database.
    * @param t          The table description model holding the name and other necessary information.
    * @return An option to a DFASDL element describing the database table.
    */
  protected def describeTable(d: org.w3c.dom.Document)(
      statements: IO[NonEmptyList[PreparedStatement]]
  )(
      t: DatabaseTable
  ): IO[Option[org.w3c.dom.Element]] =
    statements.map { stmts =>
      val DEFAULT_NUMERIC = ".*\\(([\\d-.,]+)\\)".r
      val DEFAULT_OTHER   = ".*:(.+)".r

      val metadata = {
        // Due to the nature of having to use `while` we need a buffer to fill.
        val rawMetadata   = mutable.ListBuffer.empty[RawColumnMetadata]
        val structureStmt = stmts.head
        structureStmt.setString(1, t.name)
        val structure = structureStmt.executeQuery()
        while (structure.next()) {
          val columnName = structure.getString("column_name")
          val columnType = structure.getString("data_type")

          columnType.toUpperCase(Locale.ROOT) match {
            case "TSQUERY" | "TSVECTOR" =>
            // We skip text search column types because they provide no
            // information and cause errors if written to directly.
            case _ =>
              val nullable =
                Option(structure.getString("is_nullable"))
                  .map(_.toUpperCase(Locale.ROOT) === "YES")
                  .getOrElse(false)
              val default = (
                Option(structure.getString("column_default")),
                DatabaseColumnTypeName.from(columnType).toOption.flatMap(_.toType)
              ).mapN { case (defaultString, ct) =>
                ct match {
                  case DatabaseColumnType.FormatNum | DatabaseColumnType.Num =>
                    DEFAULT_NUMERIC.findFirstIn(defaultString)
                  case _ => DEFAULT_OTHER.findFirstIn(defaultString)
                }
              }.getOrElse(None)
              val autoIncrement = Option(structure.getString("column_default"))
              val ps = for {
                ct <- DatabaseColumnTypeName.from(columnType).toOption
                t  <- ct.toType
                (p, s) = t match {
                  case DatabaseColumnType.FormatNum | DatabaseColumnType.Num =>
                    (
                      Try(structure.getLong("numeric_precision")).toOption,
                      Try(structure.getLong("numeric_scale")).toOption
                    )
                  case DatabaseColumnType.Date | DatabaseColumnType.DateTime | DatabaseColumnType.Time =>
                    (Try(structure.getLong("datetime_precision")).toOption, None)
                  case _ =>
                    (Try(structure.getLong("character_maximum_length")).toOption, None)
                }
              } yield (p, s)
              val (precision, scale) = ps.getOrElse((None, None))
              // Try to extract the foreign key information.
              val foreignKey =
                if (stmts.size > 1) {
                  val foreignKeyStmt = stmts.last
                  foreignKeyStmt.setString(1, t.name)
                  foreignKeyStmt.setString(2, columnName)
                  val fkResult = foreignKeyStmt.executeQuery()
                  if (fkResult.next()) {
                    (
                      Option(structure.getString("constraint_type")),
                      Option(structure.getString("referenced_table_name")),
                      Option(structure.getString("referenced_column_name"))
                    ).mapN { case (_, fkTableName, fkColumnName) =>
                      s"${fkTableName}_row_${fkColumnName}"
                    }
                  } else
                    None
                } else {
                  (
                    Option(structure.getString("constraint_type")),
                    Option(structure.getString("referenced_table_name")),
                    Option(structure.getString("referenced_column_name"))
                  ).mapN { case (_, fkTableName, fkColumnName) =>
                    s"${fkTableName}_row_${fkColumnName}"
                  }
                }
              rawMetadata += RawColumnMetadata(
                columnName = columnName,
                columnType = columnType,
                nullable = nullable,
                precision = precision,
                scale = scale,
                autoIncrement = autoIncrement,
                foreignKey = foreignKey,
                default = default
              )
          }
        }
        rawMetadata.toList
      }
      val tableElement = for {
        table <- DFASDLFunctions.createTableSkeletonElement(d)(t)
        rows  <- DFASDLFunctions.getFirstNode(table.getElementsByTagName(Element.ELEMENT.tagName))
        _ = metadata.map { m =>
          (
            DatabaseColumnName.from(m.columnName.toLowerCase(Locale.ROOT)),
            DatabaseColumnTypeName.from(m.columnType)
          ).mapN { case (columnName, columnType) =>
            val precision = m.precision.flatMap(p => NonNegLong.from(p).toOption)
            val scale     = m.scale.flatMap(s => NonNegLong.from(s).toOption).filterNot(_ <= 0)
            val nullable =
              if (m.nullable)
                List(Nullable)
              else
                Nil
            val autoIncrement =
              if (
                m.autoIncrement
                  .map(_.toLowerCase(Locale.ROOT).startsWith("nextval"))
                  .getOrElse(false)
              )
                List(AutoIncrement)
              else
                Nil
            val flags   = nullable ::: autoIncrement
            val fk      = m.foreignKey.flatMap(s => ForeignKey.from(s).toOption)
            val default = m.default
            val column = createColumnElement(d)(t.name)(flags)(fk)(default)(scale)(precision)(
              columnType
            )(columnName)
            column.foreach(rows.appendChild)
          }
        }
      } yield table
      tableElement
    }

  /** Prepare a list of statements that are used to fetch table details from the
    * database. In most cases the returned list will only include a single
    * prepared statement. If more statements are returned the appropriate
    * function that uses them must know how to do this. Usually they are
    * executed in the defined order (0 -> 1 -> 2).
    *
    * @param c A database connection.
    * @return A list of prepared statements.
    */
  protected def prepareDetailStatements(c: IO[Connection]): IO[NonEmptyList[PreparedStatement]] =
    c.map { connection =>
      val meta  = connection.getMetaData()
      val major = meta.getDatabaseMajorVersion()
      val minor = meta.getDatabaseMinorVersion()
      // We can use the easier LATERAL JOIN from postgresql version 9.3 onwards.
      val queries: NonEmptyList[DatabaseQueryString] =
        if (major > 9 || (major === 9 && minor >= 3))
          NonEmptyList.of(
            """SELECT * FROM (SELECT table_name, column_name, column_default, is_nullable, data_type, character_maximum_length, numeric_precision, numeric_scale, datetime_precision, ordinal_position FROM information_schema.columns WHERE UPPER(table_name) = UPPER(?)) tdetails LEFT JOIN LATERAL (SELECT tc.constraint_type, ccu.table_name AS referenced_table_name, ccu.column_name AS referenced_column_name FROM information_schema.table_constraints AS tc LEFT JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name AND UPPER(tc.constraint_type) = 'FOREIGN KEY' LEFT JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name WHERE kcu.column_name = tdetails.column_name AND UPPER(kcu.table_name) = UPPER(tdetails.table_name)) kdetails ON true ORDER BY tdetails.ordinal_position"""
          )
        else
          NonEmptyList.of(
            """SELECT table_name, column_name, column_default, is_nullable, data_type, character_maximum_length, numeric_precision, numeric_scale, datetime_precision, ordinal_position FROM information_schema.columns WHERE UPPER(table_name) = UPPER(?)""",
            """SELECT c.table_name, c.column_name, tc.constraint_type, ccu.table_name AS referenced_table_name, ccu.column_name AS referenced_column_name FROM information_schema.columns AS c JOIN information_schema.key_column_usage AS kcu ON c.column_name = kcu.column_name AND c.table_name = kcu.table_name JOIN information_schema.table_constraints AS tc ON tc.constraint_name = kcu.constraint_name AND UPPER(tc.constraint_type) = 'FOREIGN KEY' JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name WHERE UPPER(c.table_name) = UPPER(?) AND UPPER(c.column_name) = UPPER(?)"""
          )
      queries.map(q => connection.prepareStatement(q))
    }

  /** Prepare a statement that lists all user tables from the database.
    *
    * @param c A database connection.
    * @return A prepared statement which will return a list of all table names in the database.
    */
  protected def prepareListTablesStatement(c: IO[Connection]): IO[PreparedStatement] = {
    val query: DatabaseQueryString =
      """SELECT TABLES.TABLE_NAME, STRING_AGG(KEY_COLUMN_USAGE.COLUMN_NAME,',') AS PK_COLUMNS FROM INFORMATION_SCHEMA.TABLES LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ON TABLES.TABLE_CATALOG = TABLE_CONSTRAINTS.TABLE_CATALOG AND TABLES.TABLE_NAME = TABLE_CONSTRAINTS.TABLE_NAME AND UPPER(TABLE_CONSTRAINTS.CONSTRAINT_TYPE) = 'PRIMARY KEY' LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ON TABLE_CONSTRAINTS.CONSTRAINT_NAME = KEY_COLUMN_USAGE.CONSTRAINT_NAME WHERE UPPER(TABLES.TABLE_SCHEMA) = 'PUBLIC' AND UPPER(TABLES.TABLE_CATALOG) = UPPER(?) GROUP BY TABLES.TABLE_NAME ORDER BY TABLES.TABLE_NAME"""
    c.map(_.prepareStatement(query))
  }

}
