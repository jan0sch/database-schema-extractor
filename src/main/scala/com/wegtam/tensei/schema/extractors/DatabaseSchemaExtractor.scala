/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import java.net.URI
import java.nio.charset.StandardCharsets
import java.sql.DriverManager
import java.util.Properties

import cats.effect._
import cats.implicits._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.derby.DerbySchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.h2.H2SchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.hsqldb.HyperSQLSchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.mariadb.MariaDBSchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.mysql.MySQLSchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.postgresql.PostgreSQLSchemaExtractor
import com.wegtam.tensei.schema.extractors.databases.sqlite.SqliteSchemaExtractor
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString
import org.w3c.dom.Document

import scala.util.Try

object DatabaseSchemaExtractor {
  final val DB_PROPERTY_PASS: NonEmptyString = "password"
  final val DB_PROPERTY_USER: NonEmptyString = "user"

  /** Extract a schema description from a given database information and
    * return it in the format of the Data Format and Semantics Description
    * Language (DFASDL).
    *
    * @param password An optional password for the database login.
    * @param username An optional username for the database login.
    * @param url      A valid JDBC connection URL.
    * @return Either a DFASDL XML document or an error message.
    */
  def extractSchema(password: Option[JDBCPassword])(username: Option[JDBCUsername])(
      url: JDBCUrl
  ): IO[Either[ErrorMessage, Document]] = {
    val tryToExtractSchema = Try(new URI(url)).toEither match {
      case Left(e) =>
        IO {
          val error: ErrorMessage =
            ErrorMessage.from(e.getMessage()).toOption.getOrElse("Invalid JDBC URI!")
          error.asLeft[Document]
        }.some
      case Right(jdbcUrl) =>
        (DatabaseName.fromURI(jdbcUrl).toOption, SupportedDatabases.fromURI(jdbcUrl)).mapN { case (dbName, dbType) =>
          val props = new Properties()
          username.foreach(u => props.setProperty(DB_PROPERTY_USER, u))
          password.foreach(p => props.setProperty(DB_PROPERTY_PASS, new String(p, StandardCharsets.UTF_8)))
          val c = IO(DriverManager.getConnection(url, props))
          for {
            con <- c
            _ = con.getMetaData
            r <- dbType match {
              case SupportedDatabases.Derby      => DerbySchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.H2         => H2SchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.HyperSQL   => HyperSQLSchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.MariaDB    => MariaDBSchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.MySQL      => MySQLSchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.PostgreSQL => PostgreSQLSchemaExtractor.extractSchema(c)(dbName)
              case SupportedDatabases.SQLite     => SqliteSchemaExtractor.extractSchema(c)(dbName)
              case _ =>
                IO {
                  val error: ErrorMessage = "Not yet implemented!"
                  error.asLeft[Document]
                }
            }
            _ = con.close()
          } yield r
        }
    }
    val error: ErrorMessage = "Could not resolve return value from extractSchema function!"
    tryToExtractSchema.getOrElse(IO(error.asLeft[Document]))
  }

}
