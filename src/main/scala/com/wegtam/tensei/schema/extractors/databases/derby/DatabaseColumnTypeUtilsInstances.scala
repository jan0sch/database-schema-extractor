/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.derby

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {

  implicit val derbyDatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BLOB" | "BINARY LARGE OBJECT" | "CLOB" | "CHARACTER LARGE OBJECT" | "LONG VARCHAR FOR BIT DATA" =>
            Binary.some
          case "DATE"      => Date.some
          case "TIMESTAMP" => DateTime.some
          case "DEC" | "DECIMAL" | "DOUBLE" | "DOUBLE PRECISION" | "FLOAT" | "NUMERIC" | "REAL" =>
            FormatNum.some
          case "BIGINT" | "BOOLEAN" | "INT" | "INTEGER" | "SMALLINT" => Num.some
          case "CHAR" | "CHARACTER" | "CHAR VARYING" | "CHARACTER VARYING" | "LONG VARCHAR" | "VARCHAR" =>
            Str.some
          case "TIME" => Time.some
          case _      => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
