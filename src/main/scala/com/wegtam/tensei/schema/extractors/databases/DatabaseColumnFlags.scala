/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import cats.kernel.Eq
import enumeratum._

sealed trait DatabaseColumnFlags extends EnumEntry with Product with Serializable

object DatabaseColumnFlags extends Enum[DatabaseColumnFlags] {
  val values = findValues

  case object AutoIncrement extends DatabaseColumnFlags
  case object Nullable      extends DatabaseColumnFlags

  implicit val eq: Eq[DatabaseColumnFlags] = Eq.instance[DatabaseColumnFlags] { (a, b) =>
    (a, b) match {
      case (AutoIncrement, AutoIncrement) => true
      case (Nullable, Nullable)           => true
      case _                              => false
    }
  }
}
