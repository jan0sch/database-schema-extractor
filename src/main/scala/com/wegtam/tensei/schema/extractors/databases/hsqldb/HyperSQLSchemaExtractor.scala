/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.hsqldb

import java.sql.{ Connection, PreparedStatement }
import java.util.Locale

import cats.data.NonEmptyList
import cats.effect._
import cats.implicits._
import com.wegtam.tensei.schema.extractors._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnFlags._
import com.wegtam.tensei.schema.extractors.databases.hsqldb.DatabaseColumnTypeUtilsInstances._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnTypeUtils.ops._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.NonNegLong
import eu.timepit.refined.types.string.NonEmptyString
import org.dfasdl._

import scala.collection.mutable

/** A buffer for the raw metadata of a table column extracted from the
  * query.
  *
  * @param columnName    The name of the column.
  * @param columnType    The type description of the column.
  * @param nullable      A flag indicating if the column is allowed to be null.
  * @param precision     The precision (length / size) of the column.
  * @param scale         The scale of the column (will mostly be zero).
  * @param autoIncrement A flag indicating if this is an "auto increment" column.
  * @param foreignKey    The foreign key definition of the column.
  * @param defaultValue  The default value of the column.
  */
final case class RawColumnMetadata(
    columnName: String,
    columnType: String,
    nullable: Boolean,
    precision: Option[Long],
    scale: Option[Long],
    autoIncrement: Boolean,
    foreignKey: Option[String],
    default: Option[String]
)

object HyperSQLSchemaExtractor {
  final val AUTO_INC_INDICATOR: NonEmptyString = "NEXT VALUE "

  /** Extract a schema description from a given database connection and
    * return it in the format of the Data Format and Semantics Description
    * Language (DFASDL).
    *
    * @param c      A database connection.
    * @param dbName The name of the database.
    * @return Either a DFASDL XML document or an error message.
    */
  def extractSchema(
      c: IO[Connection]
  )(dbName: DatabaseName): IO[Either[ErrorMessage, org.w3c.dom.Document]] = {
    val listTables   = prepareListTablesStatement(c)
    val tableDetails = prepareDetailStatements(c)
    val document = for {
      tables <- getTables(listTables)(dbName)
      dfasdl = DFASDLFunctions.initDocument
      tablesData <- tables.traverse(t => describeTable(dfasdl)(tableDetails)(t))
      _ = tablesData.foreach(_.foreach(t => dfasdl.getDocumentElement().appendChild(t)))
    } yield dfasdl.asRight[ErrorMessage]
    document
  }

  /** Create a specific DFASDL Data-Element that represents a column of an actual table. Depending
    * on the type of the column, the Data-Element is created with the appropriate tag name and
    * additional attributes.
    *
    * @param d          The XML document.
    * @param tableName  The name of the table.
    * @param flags      A list of flags for the column (auto increment, nullable) which might be empty.
    * @param fk         The foreign key definition for the column.
    * @param default    An optional string containing the default value for the column.
    * @param scale      An optional scale of the column (e.g. 2,34 means scale 2). It will be 0 in most cases.
    * @param precision  An optional precision aka "length" of the column.
    * @param columnType The type of the column in the database table.
    * @param columnName The name of the column in the database table.
    * @return An element that represents the column.
    */
  protected def createColumnElement(d: org.w3c.dom.Document)(tableName: DatabaseTableName)(
      flags: List[DatabaseColumnFlags]
  )(fk: Option[ForeignKey])(default: Option[String])(scale: Option[NonNegLong])(
      precision: Option[NonNegLong]
  )(
      columnType: DatabaseColumnTypeName
  )(columnName: DatabaseColumnName): Option[org.w3c.dom.Element] = {
    val element = columnType.toType.map { t =>
      t match {
        case DatabaseColumnType.Binary =>
          // FIXME Actually use the binary element here!
          val e = d.createElement(Element.STRING.tagName)
          precision.foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d.show))
          e
        case DatabaseColumnType.Date =>
          val e = d.createElement(Element.DATE.tagName)
          e
        case DatabaseColumnType.DateTime =>
          val e = d.createElement(Element.DATETIME.tagName)
          e
        case DatabaseColumnType.FormatNum =>
          val e = d.createElement(Element.FORMATTED_NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach { s =>
            e.setAttribute(Attribute.MAX_PRECISION.name, s.show)
            e.setAttribute(Attribute.DECIMAL_SEPARATOR.name, ".")
          }
          precision.foreach { p =>
            e.setAttribute(Attribute.MAX_DIGITS.name, p.show)
          }
          DFASDLFunctions
            .createFormatnumRegex(DFASDLFunctions.DEFAULT_NUMERIC_SEPARATOR)(precision)(scale)
            .foreach(r => e.setAttribute(Attribute.FORMAT.name, r.show))
          e
        case DatabaseColumnType.Num =>
          val e = d.createElement(Element.NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach(s => e.setAttribute(Attribute.PRECISION.name, s.show))
          precision.foreach(p => e.setAttribute(Attribute.MAX_DIGITS.name, p.show))
          e
        case DatabaseColumnType.Str =>
          val e = d.createElement(Element.STRING.tagName)
          precision.foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d))
          e
        case DatabaseColumnType.Time =>
          val e = d.createElement(Element.TIME.tagName)
          e
      }
    }
    element.map { e =>
      e.setAttribute(DFASDLFunctions.ID_ATTR, s"${tableName.show}_row_${columnName.show}")
      e.setAttribute(Attribute.DB_COLUMN_NAME.name, columnName.show)
      flags
        .find(_ === DatabaseColumnFlags.AutoIncrement)
        .foreach(_ => e.setAttribute(Attribute.DB_AUTO_INCREMENT.name, "true"))
      fk.foreach(cs => e.setAttribute(Attribute.DB_FOREIGN_KEY.name, cs.show.toLowerCase(Locale.ROOT)))
      e
    }
  }

  /** Return a list of tables using the given prepared statement and
    * database name.
    *
    * @param s A prepared statement to list all tables.
    * @param n The name of the database.
    * @return A list of database table description which can be used to extract more details from the schema.
    */
  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  protected def getTables(s: IO[PreparedStatement])(n: DatabaseName): IO[List[DatabaseTable]] =
    s.map { stmt =>
      val ts = mutable.ListBuffer.empty[DatabaseTable]
      if (stmt.getParameterMetaData().getParameterCount() > 0) {
        stmt.setString(1, n)
      }
      val rs   = stmt.executeQuery()
      val meta = rs.getMetaData()
      while (rs.next()) {
        val tn = rs.getString("TABLE_NAME").toLowerCase(Locale.ROOT)
        val pk =
          if (meta.getColumnCount() > 1 && rs.getString("PK_COLUMNS") =!= null)
            rs.getString("PK_COLUMNS").toLowerCase(Locale.ROOT).split(",").toList
          else
            List.empty
        DatabaseTable.from(tn, pk).foreach(t => ts += t)
      }
      ts.toList
    }

  /** Create a description of a database table within an HyperSQL database.
    *
    * @param d          A DFASDL document that will be used for the table description.
    * @param statements A list prepared SQL statements that are used to extract necessary information from the database.
    * @param t          The table description model holding the name and other necessary information.
    * @return An option to a DFASDL element describing the database table.
    */
  protected def describeTable(d: org.w3c.dom.Document)(
      statements: IO[NonEmptyList[PreparedStatement]]
  )(
      t: DatabaseTable
  ): IO[Option[org.w3c.dom.Element]] =
    statements.map { stmts =>
      val metadata = {
        // Due to the nature of having to use `while` we need a buffer to fill.
        val rawMetadata   = mutable.ListBuffer.empty[RawColumnMetadata]
        val tableInfoStmt = stmts.head
        tableInfoStmt.setString(1, t.name)
        val tableInfo = tableInfoStmt.executeQuery()
        while (tableInfo.next()) {
          val columnName = tableInfo.getString("COLUMN_NAME")
          val columnType = tableInfo.getString("TYPE_NAME")
          val precision = tableInfo.getLong(
            "COLUMN_SIZE"
          ) // FIXME For column type `DECIMAL` or `NUMERIC` we should use "NUM_PREC_RADIX" here but that holds a wrong value!
          val scale =
            if (columnType == "DECIMAL" || columnType == "NUMERIC")
              tableInfo.getLong("DECIMAL_DIGITS")
            else
              0L
          val nullable      = Option(tableInfo.getString("NULLABLE")).exists(_ == "YES")
          val columnDefault = Option(tableInfo.getString("COLUMN_DEF"))
          val autoIncrement = Option(tableInfo.getString("AUTOINC")).exists(_ == "YES")
          val foreignKey =
            (Option(tableInfo.getString("PKTABLE_NAME")), Option(tableInfo.getString("PKCOLUMN_NAME"))).mapN {
              case (pkTableName, pkColumnName) => s"${pkTableName}_row_${pkColumnName}"
            }
          rawMetadata += RawColumnMetadata(
            columnName = columnName,
            columnType = columnType,
            nullable = nullable,
            precision = precision.some,
            scale = scale.some,
            autoIncrement = autoIncrement,
            foreignKey = foreignKey,
            default = columnDefault
          )
        }
        rawMetadata.toList
      }
      val tableElement = for {
        table <- DFASDLFunctions.createTableSkeletonElement(d)(t)
        rows  <- DFASDLFunctions.getFirstNode(table.getElementsByTagName(Element.ELEMENT.tagName))
        _ = metadata.map { m =>
          (
            DatabaseColumnName.from(m.columnName.toLowerCase(Locale.ROOT)),
            DatabaseColumnTypeName.from(m.columnType)
          ).mapN { case (columnName, columnType) =>
            val precision = m.precision.flatMap(p => NonNegLong.from(p).toOption)
            val scale     = m.scale.flatMap(s => NonNegLong.from(s).toOption).filterNot(_ <= 0)
            val nullable =
              if (m.nullable)
                List(Nullable)
              else
                Nil
            val autoIncrement =
              if (m.autoIncrement)
                List(AutoIncrement)
              else
                Nil
            val flags   = nullable ::: autoIncrement
            val fk      = m.foreignKey.flatMap(s => ForeignKey.from(s).toOption)
            val default = m.default.filterNot(_.startsWith(AUTO_INC_INDICATOR))
            val column = createColumnElement(d)(t.name)(flags)(fk)(default)(scale)(precision)(
              columnType
            )(columnName)
            column.foreach(rows.appendChild)
          }
        }
      } yield table
      tableElement
    }

  /** Prepare a list of statements that are used to fetch table details from the
    * database. In most cases the returned list will only include a single
    * prepared statement. If more statements are returned the appropriate
    * function that uses them must know how to do this. Usually they are
    * executed in the defined order (0 -> 1 -> 2).
    *
    * @param c A database connection.
    * @return A list of prepared statements.
    */
  protected def prepareDetailStatements(c: IO[Connection]): IO[NonEmptyList[PreparedStatement]] = {
    val queries: NonEmptyList[DatabaseQueryString] = NonEmptyList.of(
      """SELECT c.COLUMN_NAME AS COLUMN_NAME, UPPER(c.TYPE_NAME) AS TYPE_NAME, c.COLUMN_DEF AS COLUMN_DEF, c.COLUMN_SIZE AS COLUMN_SIZE, c.DECIMAL_DIGITS AS DECIMAL_DIGITS, c.NUM_PREC_RADIX AS NUM_PREC_RADIX, UPPER(c.IS_NULLABLE) AS NULLABLE, UPPER(c.IS_AUTOINCREMENT) AS AUTOINC, LOWER(fks.PKTABLE_NAME) AS PKTABLE_NAME, LOWER(fks.PKCOLUMN_NAME) AS PKCOLUMN_NAME FROM INFORMATION_SCHEMA.SYSTEM_COLUMNS AS c LEFT JOIN INFORMATION_SCHEMA.SYSTEM_CROSSREFERENCE AS fks ON c.TABLE_CAT = fks.FKTABLE_CAT AND c.TABLE_SCHEM = fks.FKTABLE_SCHEM AND c.TABLE_NAME = fks.FKTABLE_NAME AND c.COLUMN_NAME = fks.FKCOLUMN_NAME WHERE UPPER(c.TABLE_NAME) = UPPER(?) AND UPPER(c.TABLE_SCHEM) = 'PUBLIC' ORDER BY c.ORDINAL_POSITION"""
    )
    queries.traverse(q => c.map(_.prepareStatement(q)))
  }

  /** Prepare a statement that lists all user tables from the database.
    *
    * @param c A database connection.
    * @return A prepared statement which will return a list of all table names in the database.
    */
  protected def prepareListTablesStatement(c: IO[Connection]): IO[PreparedStatement] = {
    val query: DatabaseQueryString =
      """SELECT t.TABLE_NAME AS TABLE_NAME, GROUP_CONCAT(pks.COLUMN_NAME) AS PK_COLUMNS FROM INFORMATION_SCHEMA.SYSTEM_TABLES AS t LEFT JOIN INFORMATION_SCHEMA.SYSTEM_PRIMARYKEYS AS pks ON t.TABLE_CAT = pks.TABLE_CAT AND t.TABLE_SCHEM = pks.TABLE_SCHEM AND t.TABLE_NAME = pks.TABLE_NAME WHERE UPPER(t.TABLE_TYPE) = 'TABLE' GROUP BY t.TABLE_NAME"""
    c.map(_.prepareStatement(query))
  }

}
