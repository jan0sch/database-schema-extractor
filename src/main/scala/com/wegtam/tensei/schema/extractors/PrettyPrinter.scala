/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import java.io.{ StringWriter, Writer }
import javax.xml.transform.{ OutputKeys, TransformerFactory }
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

import simulacrum._
import org.w3c.dom.{ Document, Element, Node }

/** A typeclass for pretty printing arbitrary types.
  */
@typeclass trait PrettyPrinter[A] {

  /** Construct a pretty printed string representation of the given type A.
    *
    * @param a An arbitrary object.
    * @return A pretty formatted string representation.
    */
  def toPrettyString(a: A): String

}

object PrettyPrinterInstances {

  implicit val prettyPrintDocument: PrettyPrinter[Document] = new PrettyPrinter[Document] {
    override def toPrettyString(a: Document): String = {
      val tf = TransformerFactory.newInstance().newTransformer()
      tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8")
      tf.setOutputProperty(OutputKeys.INDENT, "yes")

      val out: Writer = new StringWriter()
      tf.transform(new DOMSource(a), new StreamResult(out))
      out.toString
    }
  }

  implicit val prettyPrintElement: PrettyPrinter[Element] = new PrettyPrinter[Element] {
    override def toPrettyString(a: Element): String = {
      val attrs = for (i <- 0 until a.getAttributes().getLength()) yield {
        val attrName  = a.getAttributes.item(i).getNodeName
        val attrValue = a.getAttribute(attrName)
        s"""$attrName="$attrValue""""
      }
      s"""<${a.getTagName()} ${attrs.mkString(" ")}>"""
    }
  }

  implicit val prettyPrintNode: PrettyPrinter[Node] = new PrettyPrinter[Node] {
    @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
    override def toPrettyString(a: Node): String = {
      val e = a.asInstanceOf[Element]
      val attrs = for (i <- 0 until e.getAttributes().getLength()) yield {
        val attrName  = e.getAttributes.item(i).getNodeName
        val attrValue = e.getAttribute(attrName)
        s"""$attrName="$attrValue""""
      }
      s"""<${e.getTagName()} ${attrs.mkString(" ")}>"""
    }
  }

}
