/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema

import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

package object extractors {

  type ApplicationName = String Refined NonEmpty
  object ApplicationName extends RefinedTypeOps[ApplicationName, String] with CatsRefinedTypeOpsSyntax

  type ApplicationHeader = String Refined NonEmpty
  object ApplicationHeader extends RefinedTypeOps[ApplicationHeader, String] with CatsRefinedTypeOpsSyntax

  type ApplicationVersion =
    String Refined MatchesRegex[W.`"^\\\\d+\\\\.\\\\d+\\\\.\\\\d+[\\\\w-]*$"`.T]
  object ApplicationVersion extends RefinedTypeOps[ApplicationVersion, String] with CatsRefinedTypeOpsSyntax

  type ErrorMessage = String Refined NonEmpty
  object ErrorMessage extends RefinedTypeOps[ErrorMessage, String] with CatsRefinedTypeOpsSyntax
}
