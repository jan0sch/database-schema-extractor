/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import cats.data.NonEmptyList
import cats.implicits._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._

/** Functions to create ANSI SQL statements for certain use cases.
  */
object CreateSQLStatements {

  /** Create an ANSI SQL SELECT COUNT(*) statement the check if an entry
    * exists within the database.
    *
    * @param t    The name of the database table.
    * @param cols A list of column names that should be used in the `WHERE` clause.
    * @return A string holding the select count statement.
    */
  def createCountStatement(
      t: DatabaseTableName
  )(cols: NonEmptyList[DatabaseColumnName]): DatabaseQueryString =
    DatabaseQueryString.unsafeFrom(
      s"""SELECT COUNT(*) FROM ${t.show} WHERE ${cols
        .map(col => s"${col.show} = ?")
        .toList
        .mkString(" AND ")}"""
    )

  /** Create an ANSI SQL INSERT statement from the given parameters.
    *
    * @param t           The name of the database table.
    * @param cols        A list of column names.
    * @param autoIncCols A list of column names that are auto increment columns that might be empty.
    * @return A string holding the insert statement.
    */
  def createInsertStatement(t: DatabaseTableName)(
      cols: NonEmptyList[DatabaseColumnName]
  )(autoIncCols: List[DatabaseColumnName]): DatabaseQueryString = {
    val valuePlaceholders = cols.map(c => if (autoIncCols.contains(c)) "DEFAULT" else "?")
    DatabaseQueryString.unsafeFrom(
      s"""INSERT INTO ${t.show} (${cols.toList.mkString(", ")}) VALUES(${valuePlaceholders.toList
        .mkString(", ")})"""
    )
  }

  /** Create an ANSI SQL UPDATE statement from the given parameters.
    *
    * @param t          The name of the database table.
    * @param cols       A list of column names.
    * @param primaryKey A list of primary key column names.
    * @return A string holding the update statement.
    */
  def createUpdateStatement(t: DatabaseTableName)(
      cols: NonEmptyList[DatabaseColumnName]
  )(primaryKey: NonEmptyList[DatabaseColumnName]): DatabaseQueryString =
    DatabaseQueryString.unsafeFrom(
      s"""UPDATE ${t.show} SET ${cols
        .map(col => s"${col.show} = ?")
        .toList
        .mkString(", ")} WHERE ${primaryKey
        .map(col => s"${col.show} = ?")
        .toList
        .mkString(" AND ")}"""
    )

}
