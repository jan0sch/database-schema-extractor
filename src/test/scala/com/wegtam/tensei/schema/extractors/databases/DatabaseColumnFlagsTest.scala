/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import org.scalacheck.{ Arbitrary, Gen }
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class DatabaseColumnFlagsTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  private val columnFlag: Gen[DatabaseColumnFlags]                         = Gen.oneOf(DatabaseColumnFlags.values)
  private implicit val arbitraryColumnFlag: Arbitrary[DatabaseColumnFlags] = Arbitrary(columnFlag)

  "Eq instance" must {
    "return false for different flags" in {
      forAll("a", "b") { (a: DatabaseColumnFlags, b: DatabaseColumnFlags) =>
        whenever(!(a eq b)) {
          DatabaseColumnFlags.eq.eqv(a, b) must be(false)
        }
      }
    }

    "return true for equal flags" in {
      forAll("flag") { a: DatabaseColumnFlags =>
        DatabaseColumnFlags.eq.eqv(a, a) must be(true)
      }
    }
  }

}
