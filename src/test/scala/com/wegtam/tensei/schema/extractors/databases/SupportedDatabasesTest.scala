/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import java.net.URI

import com.wegtam.tensei.schema.extractors.databases.SupportedDatabases._
import org.scalacheck.Gen
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class SupportedDatabasesTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  private val expectedDbName: String = "my-database"
  private val genSupportedURI: Gen[(URI, SupportedDatabases)] = Gen.oneOf(
    List(
      (new URI(s"jdbc:derby:/var/db/derby/$expectedDbName;create=true")          -> Derby),
      (new URI(s"jdbc:firebirdsql://localhost:12345/$expectedDbName")            -> Firebird),
      (new URI(s"jdbc:firebirdsql://localhost/$expectedDbName")                  -> Firebird),
      (new URI(s"jdbc:firebirdsql:$expectedDbName")                              -> Firebird),
      (new URI(s"jdbc:h2:/var/db/h2/$expectedDbName")                            -> H2),
      (new URI(s"jdbc:hsqldb:mem:$expectedDbName")                               -> HyperSQL),
      (new URI(s"jdbc:hsqldb:file:/var/db/hsqldb/$expectedDbName")               -> HyperSQL),
      (new URI(s"jdbc:hsqldb:res:/com/wegtam/tensei/$expectedDbName")            -> HyperSQL),
      (new URI(s"jdbc:hsqldb:hsql://localhost/$expectedDbName")                  -> HyperSQL),
      (new URI(s"jdbc:hsqldb:hsqls://127.0.0.1:12345/$expectedDbName")           -> HyperSQL),
      (new URI(s"jdbc:hsqldb:http://dbserver.example.com/$expectedDbName")       -> HyperSQL),
      (new URI(s"jdbc:hsqldb:https://dbserver.example.com/$expectedDbName")      -> HyperSQL),
      (new URI(s"jdbc:mariadb://localhost:12345/$expectedDbName")                -> MariaDB),
      (new URI(s"jdbc:mysql://localhost:12345/$expectedDbName")                  -> MySQL),
      (new URI(s"jdbc:oracle:oci:@$expectedDbName")                              -> Oracle),
      (new URI(s"jdbc:oracle:oci8:@$expectedDbName")                             -> Oracle),
      (new URI(s"jdbc:oracle:thin:@$expectedDbName")                             -> Oracle),
      (new URI(s"jdbc:oracle:thin:@localhost:$expectedDbName")                   -> Oracle),
      (new URI(s"jdbc:oracle:thin:@localhost:12345:$expectedDbName")             -> Oracle),
      (new URI(s"jdbc:postgresql://localhost:12345/$expectedDbName")             -> PostgreSQL),
      (new URI(s"jdbc:sqlite:/var/db/sqlite/$expectedDbName")                    -> SQLite),
      (new URI(s"jdbc:sqlite::memory:$expectedDbName")                           -> SQLite),
      (new URI(s"jdbc:sqlserver://localhost:12345;databaseName=$expectedDbName") -> SQLServer)
    )
  )
  private val genNotSupportedURI: Gen[URI] =
    Gen.alphaNumStr.map(str => new URI(s"jdbc:$str:/$expectedDbName"))

  "fromURI" when {
    "URI contains a supported database" must {
      "return the correct database" in {
        forAll((genSupportedURI, "JDBC URI")) { t: (URI, SupportedDatabases) =>
          val (uri, db) = t
          SupportedDatabases.fromURI(uri) must contain(db)
        }
      }
    }

    "URI does not contain a supported database" must {
      "return an empty Option" in {
        forAll((genNotSupportedURI, "JDBC URI")) { u: URI =>
          SupportedDatabases.fromURI(u) must be(empty)
        }
      }
    }

    "URI is empty" must {
      "return an empty Option" in {
        SupportedDatabases.fromURI(new URI("")) must be(empty)
      }
    }
  }

}
