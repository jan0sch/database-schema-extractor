/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import java.io.IOException
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes

import cats.instances.string._
import cats.syntax.eq._

import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.xmlunit.diff._

import scala.jdk.CollectionConverters._

/** Base template for our integration tests.
  */
abstract class IntegrationSpec extends AnyWordSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  /** Delete the given directory recursively.
    *
    * @param path A path.
    * @return `true` if the directory was deleted.
    */
  protected def deleteDirectory(path: Path): Boolean =
    if (path.toString.trim =!= "/") {
      Files.walkFileTree(
        path,
        new FileVisitor[Path] {
          override def visitFileFailed(file: Path, exc: IOException): FileVisitResult =
            FileVisitResult.CONTINUE

          override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
            Files.delete(file)
            FileVisitResult.CONTINUE
          }

          override def preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult =
            FileVisitResult.CONTINUE

          override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
            Files.delete(dir)
            FileVisitResult.CONTINUE
          }
        }
      )
      Files.deleteIfExists(path)
    } else
      false

  /** Return a list of differences from the fiven XML unit diff.
    *
    * @param d An XML unit diff.
    * @return A list with the detailed differences which might be empty.
    */
  protected def xmlDiffDetails(d: Diff): List[Difference] =
    d.getDifferences().asScala.toList

}
